<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css" />

	<!--<link rel="stylesheet" href="{{URL::asset('UI/one-page/css/et-line.css')}}" type="text/css" />-->

	<link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!--<link rel="stylesheet" href="{{URL::asset('UI/css/colors.php?color=fcc741')}}" type="text/css" />-->

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="{{URL::asset('UI/course/css/fonts.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/course/course.css')}}" type="text/css" />

	<!-- Document Title
	============================================= -->
	<title>{{$title}}</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		
<div class="modal1 mfp-hide" id="modal-register">
			<div class="card divcenter" style="max-width: 540px;">
				<div class="card-header py-3 nobg center">
					<h3 class="mb-0 t400">Hello, Welcome Back</h3>
				</div>
				<div class="card-body divcenter py-5" style="max-width: 70%;">
<form id="login-form" name="login-form" class="nobottommargin row" action="#" method="post">

	<div class="col-12"><select class="selectpicker form-control not-dark" tabindex="-98">
											<option>Select User Type</option>
											<option>Student</option>
											<option>Firm</option>
										</select></div>
	
					<div class="divider divider-center"><span class="position-relative" style="top: -2px">AND</span></div>

					

						<div class="col-12">
							<input type="text" id="login-form-username" name="login-form-username" value="" class="form-control not-dark" placeholder="Username" />
						</div>

						<div class="col-12 mt-4">
							<input type="password" id="login-form-password" name="login-form-password" value="" class="form-control not-dark" placeholder="Password" />
						</div>

						<div class="col-12">
							<a href="#" class="fright text-dark t300 mt-2">Forgot Password?</a>
						</div>

						<div class="col-12 mt-4">
							<button class="button button-rounded btn-block nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
						</div>
					</form>
				</div>
				<div class="card-footer py-4 center">
					<p class="mb-0">Don't have an account? <a href="#"><u>Sign up</u></a></p>
				</div>
			</div>
		</div>
		<!-- Top Bar
		============================================= -->
		
		@include('UI.common.top_bar')
		<!-- Header
		============================================= -->
		@include('UI.common.header')
		<!-- #header end -->
		
		<!-- Content
		============================================= -->
		<!-- Page Title
		============================================= 
		<section id="page-title">

			<div class="container clearfix">
				<h1>My Account</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">Login</li>
				</ol>
			</div>

		</section>--><!-- #page-title end -->

		<!-- Content
		============================================= -->
		@yield('Content')
		<!-- #content end -->

		<!-- Footer
		============================================= -->
		@include('UI.common.footer')
		<!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{URL::asset('UI/js/jquery.js')}}"></script>
	<script src="{{URL::asset('UI/js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{URL::asset('UI/js/functions.js')}}"></script>

</body>
</html>