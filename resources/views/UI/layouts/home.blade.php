@extends('UI.base')
@section('Content')
<section id="content">

    <div class="content-wrap">

        

        <div class="container clearfix">

            <div class="col_one_third nobottommargin">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                    <!--<div class="fbox-icon">
                        <a href="#"><i class="i-alt noborder icon-shop"></i></a>
                    </div>-->
                    <h3>Starting &amp; Managing Your Business has never been easier!<span class="subtitle">Start a Company, Income Tax registration &amp; Filings</span></h3>
                    <a href="#" class="button button-border button-border-thin button-palegreen">Enroll now</a>
                </div>
            </div>

            <div class="col_one_third nobottommargin feature_call">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                    
                    <h3>Automated All-in-One Business Solution<span class="subtitle">*Terms And Conditions apply</span></h3>
                    <a href="#" class="button button-3d button-large button-rounded button-orange">Call Now: 91-86184 14801</a>
                </div>
            </div>

            

            <div class="col_one_third nobottommargin col_last">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                    <h3>Expert Assisted Tax Filing <i>@ 799</i><span class="subtitle">A proven step-by-step framework to help you launch the next big thing</span></h3>
                    <a href="#" class="button button-border button-border-thin button-palegreen">Get Started</a>
                </div>
            </div>

            <div class="clear"></div>
            

        </div>
        
        <div class="container clearfix team">

            <div class="heading-block left">
                    <h2>Freelance Accounting Professionals</h2>
                    <div class="bar" style="margin: 5px 0;"></div>
                    <span>Digital ledgers to keep business accounts balanced</span>
                </div>
            <!-- ======= Freelancer Section ======= -->


<div class="row">
    
    <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-autoplay="3000" data-nav="true" data-loop="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-lg="3" data-items-xl="4">

                <div class="oc-item">							
                    <a href="https://yatriclasses.skillsgroom.com"><img src="{{URL::asset('UI/images/trainers/male.jpg')}}" alt="Image 1" class="cropped" title="Click to view"></a>
                    <div class="freelance freelance-content">
        <h4>Name - <i>Qualification</i></h4>
        <h5>Location </h5>	
<div>Experience - Years</div>
                        <div>Services</div>

                    </div>
                    <!--<div class="center mt-3"><a href="https://yatriclasses.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me</a></div>-->
                </div>
                <div class="oc-item">
                    <a href="https://acceleratedacademy.skillsgroom.com"><img src="{{URL::asset('UI/images/trainers/male.jpg')}}" alt="Image 2" class="cropped" title="Click to view"></a>
                    <div class="freelance freelance-content">
        <h4>Name - <i>Qualification</i></h4>
        <h5>Location </h5>	
<div>Experience - Years</div>
                        <div>Services</div>

                    </div>
                    
                </div>
                <div class="oc-item">
                    <a href="https://acceleratedacademy.skillsgroom.com"><img src="{{URL::asset('UI/images/trainers/male.jpg')}}" alt="Image 3" class="cropped" title="Click to view"></a>
                    <div class="freelance freelance-content">
        <h4>Name - <i>Qualification</i></h4>
        <h5>Location </h5>	
<div>Experience - Years</div>
                        <div>Services</div>

                    </div>
                    
                </div>
                <div class="oc-item">
                    <a href="https://knowledgeseekers.skillsgroom.com"><img src="{{URL::asset('UI/images/trainers/male.jpg')}}" alt="Image 4" class="cropped"  title="Click to view"></a>
                    <div class="freelance freelance-content">
        <h4>Name - <i>Qualification</i></h4>
        <h5>Location </h5>	
<div>Experience - Years</div>
                        <div>Services</div>

                    </div>
                    
                </div>
                <div class="oc-item">
                    <a href="https://glowwormacademy.skillsgroom.com"><img src="{{URL::asset('UI/images/trainers/male.jpg')}}" alt="Image 5" class="cropped"  title="Click to view"></a>
                    <div class="freelance freelance-content">
        <h4>Name - <i>Qualification</i></h4>
        <h5>Location </h5>	
<div>Experience - Years</div>
                        <div>Services</div>

                    </div>
                    
                </div>

            </div>
    
    
            <div class="container clearfix mt-1">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 open_frm_bx loaded" data-background-image-url="images/openform_rt.jpg" style="background-image: url({{URL::asset('UI/images/openform_rt.jpg')}});
background-repeat: no-repeat;
background-position: right bottom;
border: 1px solid #efefef;
background-color: #fff;padding:20px 0 0 20px;box-shadow: 0 0 8px 0 rgba(0,0,0,0.2)">
    <h3>Find the freelancing accountant near you</h3>
    <!--<span>Structure your learning and get a certificate to prove it.</span>-->
    <form action="/freelancer/search" method="post" class="landing-wide-form clearfix mt-0">
        @csrf
                    <div class="col_four_fifth nobottommargin">
                        <div class="col_one_third nobottommargin mr-2">
                            <select class="form-control form-control-lg not-dark" id="exampleFormControlSelect1" value="" name="country">
                                <option>Country</option>
                                @foreach($Users as $User)
                                    <option value="{{$User->country}}">{{$User->country}}</option>
                                @endforeach
                            </select>									
                        </div>
                        <div class="col_one_third nobottommargin mr-2">
                            <select class="form-control form-control-lg not-dark" id="exampleFormControlSelect3" value="" name="state">
                                <option>State</option>
                                @foreach($Users as $User)
                                    <option value="{{$User->state}}">{{$User->state}}</option>
                                @endforeach
                            </select>									
                        </div>
                        <div class="col_one_third nobottommargin mr-2">
                            <select class="form-control form-control-lg not-dark" id="exampleFormControlSelect2" value="" name="city">
                                <option>City</option>
                                @foreach($Users as $User)
                                    <option value="{{$User->city}}">{{$User->city}}</option>
                                @endforeach
                            </select>									
                        </div>
                    </div>
                    <div class="col_one_fifth col_last nobottommargin">
                        <!--<button class="btn btn-lg btn-warning btn-block nomargin" value="submit" type="submit">Search</button>-->
                        <button type="submit" class="button button-3d button-rounded button-orange mt-0"><i class="icon-ok"></i>Search</button>
                    </div>
                    </form>
</div>
            </div>
        
</div>


<!-- End Freelancer Section -->

        </div>

        <div class="section topmargin-lg">
            <div class="container clearfix">
<div class="postcontent nobottommargin clearfix">
                <div class="heading-block left">
                    <h2>Online Training And Certification Courses</h2>
                    <div class="element"></div>
                    <div class="bar" style="margin: 5px 0;"></div>
                    <span>Empower yourself with the world’s leading experts.</span>
                </div>
<!--Training Carousel--><div  class="training">
<div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">
                    
<!--Training Item-->
                <div class="oc-item">
                    <div class="ipost clearfix">
                    
    <div class="testimonial-item">
      <img src="{{URL::asset('UI/images/training/training1.jpg')}}" class="testimonial-img" alt="">
        <div class="training_content">
      <h2>4 Months Certification Course for B.Com freshers</h2>
      <h3>By Accountswale</h3>

      <p><strong>Training Duration -</strong> 4 Months, 100% Live Online Training</p>

            <p><strong>Key takeaways -</strong> This course covers training on accounting softwares -Tally and Quickbooks, PF/ESI/Labour Act, Basics of Payroll, GST filing and how to perform Accounts Executive Role effectively in the organisation</p>

      <p><strong>Training -</strong> Friday, Saturday and Sunday<br>
          <strong>Batch Time -</strong> 10 AM to 12 PM OR 6 PM to 8 PM</p>

            <p><strong>Training Fee -</strong> Rs. 8000</p>

        <div class="clear"></div>
        
        
        <div class="social">
            <a href=""><i class="icon-facebook facebook"></i></a>
            <a href=""><i class="icon-twitter twitter"></i></a>
            <a href=""><i class="icon-linkedin-in linkedin"></i></a>
        </div>
            <div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
        </div>
    </div>
                        </div>
                    </div>
        <div class="oc-item">
                    <div class="ipost clearfix">
                    
    <div class="testimonial-item">
      <img src="{{URL::asset('UI/images/training/training2.jpg')}}" class="testimonial-img" alt="">
        <div class="training_content">
      <h2>GSTR 1 , GSTR 3B and GSTR 9 Filing</h2>
      <h3>By CA Nihalchand Jain</h3>

      <p><strong>Training Duration -</strong> 60 Minutes</p>

            <p><strong>Key takeaways -</strong> GST Returns filing - GSTR 1 and GSTR 3B, , Q&A and Participation certificate</p>

      <p><strong>Training -</strong> Sunday<br>
          <strong>Batch Time -</strong> 3:30 to 5:00 PM</p>

            <p><strong>Training Fee -</strong> Rs. 199</p>

        <div class="clear"></div>
        
        
        <div class="social">
            <a href=""><i class="icon-facebook facebook"></i></a>
            <a href=""><i class="icon-twitter twitter"></i></a>
            <a href=""><i class="icon-linkedin-in linkedin"></i></a>
        </div>
            <div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
        </div>
    </div>
                        </div>
                    </div>
        <div class="oc-item">
                    <div class="ipost clearfix">
                    
    <div class="testimonial-item">
      <img src="{{URL::asset('UI/images/training/training3.png')}}" class="testimonial-img" alt="">
        <div class="training_content">
      <h2>PF/ESI/Labour Act Certification Training</h2>
      <h3>By Accountswale</h3>

      <p><strong>Training Duration -</strong> 4 Hours</p>

            <p><strong>Key takeaways:</strong> PF/ESI/Labour Act rules , registration process,applicability and Returns filing . General question and answers , Participation certificate</p>

      <p><strong>Training -</strong> Sunday<br>
          <strong>Batch Time -</strong> 10 AM to 2 PM</p>

            <p><strong>Training Fee -</strong> Rs. 899</p>

        <div class="clear"></div>
        
        
        <div class="social">
            <a href=""><i class="icon-facebook facebook"></i></a>
            <a href=""><i class="icon-twitter twitter"></i></a>
            <a href=""><i class="icon-linkedin-in linkedin"></i></a>
        </div>
            <div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
        </div>
    </div>
                        </div>
                    </div>
        <div class="oc-item">
                    <div class="ipost clearfix">
                    
    <div class="testimonial-item">
      <img src="{{URL::asset('UI/images/training/training4.jpg')}}" class="testimonial-img" alt="">
        <div class="training_content">
      <h2>3 month weekend GST Detailed Certification Course</h2>
      <h3>By CA Nihalchand Jain</h3>

      <p><strong>Training Duration -</strong> 3 Months (Duration -51 Hours )</p>

            <p><strong>Key takeaways:</strong> This course will cover: basics of GST that gives the attendee a basic idea about the following topics: a. Meaning of supply b. Time of supply c. Value of supply d. Place of supply e. Input Tax Credit f. Registration. 100% Placement assistance. </p>

      <p><strong>Training -</strong> Saturday and Sunday<br>
          <strong>Batch Time -</strong> 3:30 PM to 5:00 PM</p>

            <p><strong>Training Fee -</strong> Rs. 4999</p>

        <div class="clear"></div>
        
        
        <div class="social">
            <a href=""><i class="icon-facebook facebook"></i></a>
            <a href=""><i class="icon-twitter twitter"></i></a>
            <a href=""><i class="icon-linkedin-in linkedin"></i></a>
        </div>
            <div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
        </div>
    </div>
                        </div>
                    </div>
                
<div class="oc-item">
                    <div class="ipost clearfix">
                    
    <div class="testimonial-item">
      <img src="{{URL::asset('UI/images/training/training5.jpg')}}" class="testimonial-img" alt="">
        <div class="training_content">
      <h2>1 Week Tally Live Coaching</h2>
      <h3>By Accountswale</h3>

      <p><strong>Training Duration -</strong> 1 Week</p>

            <p><strong>Key takeaways:</strong>  Learn Basics of Tally . This course is for beginners who wants to learn Tally to manage Accounting job. By attending this course you can expect learning how to maintain accounting using tally software.</p>

      <p><strong>Training -</strong> Monday to Saturday<br>
          <strong>Batch Time -</strong> 9:30 AM to 11:30 AM</p>

            <p><strong>Training Fee -</strong> Rs. 499</p>

        <div class="clear"></div>
        
        
        <div class="social">
            <a href=""><i class="icon-facebook facebook"></i></a>
            <a href=""><i class="icon-twitter twitter"></i></a>
            <a href=""><i class="icon-linkedin-in linkedin"></i></a>
        </div>
            <div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
        </div>
    </div>
                        </div>
                    </div>
                
                
                

            </div>
</div>
                </div>
                <div class="sidebar nobottommargin col_last clearfix">
                    <img src="{{URL::asset('UI/images/ad_banner.png')}}" alt=""/>
                </div>
                <div class="clear bottommargin-sm"></div>


            </div>
        </div>
        
        
        <section class="firms">
            <div class="container clearfix">
                <!--<div class="tabs tabs-bordered clearfix ui-tabs ui-corner-all ui-widget ui-widget-content" id="tab-2">

                    <ul class="tab-nav clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="tabs-5" aria-labelledby="ui-id-5" aria-selected="true" aria-expanded="true"><a href="#tabs-5" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-5">For CA Firms</a></li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="tabs-6" aria-labelledby="ui-id-6" aria-selected="false" aria-expanded="false"><a href="#tabs-6" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-6">For Corporates</a></li>								
                        <li class="hidden-phone ui-tabs-tab ui-corner-top ui-state-default ui-tab" role="tab" tabindex="-1" aria-controls="tabs-7" aria-labelledby="ui-id-7" aria-selected="false" aria-expanded="false"><a href="#tabs-7" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-7">Individuals</a></li>
                    </ul>

                    <div class="tab-container">

                        <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-5" aria-labelledby="ui-id-5" role="tabpanel" aria-hidden="false">
                            Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.
                        </div>
                        <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-6" aria-labelledby="ui-id-6" role="tabpanel" style="display: none;" aria-hidden="true">
                            Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
                        </div>								
                        <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-7" aria-labelledby="ui-id-7" role="tabpanel" style="display: none;" aria-hidden="true">
                            Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
                        </div>

                    </div>

                </div>-->
                <div class="col_one_third nobottommargin">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="background:#fff;">
                    <h3>Starting &amp; Managing Your Business has never been easier!<span class="subtitle">Start a Company, Income Tax registration &amp; Filings</span></h3>
                    <a href="#" class="button button-border button-border-thin button-palegreen">Enroll now</a>
                </div>
            </div>

            <div class="col_one_third nobottommargin feature_call">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">							
                    <h3>Automated All-in-One Business Solution<span class="subtitle">*Terms And Conditions apply</span></h3>
                    <a href="#" class="button button-3d button-large button-rounded button-orange">Call Now: 91-86184 14801</a>
                </div>
            </div>

            

            <div class="col_one_third nobottommargin col_last">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="background:#fff;">
                    <h3>Expert Assisted Tax Filing <i>@ 799</i><span class="subtitle">A proven step-by-step framework to help you launch the next big thing</span></h3>
                    <a href="#" class="button button-border button-border-thin button-palegreen">Get Started</a>
                </div>
            </div>

            <div class="clear"></div>
            </div>
        </section>
        <section class="video_trainer">
        <div class="container clearfix">
<div class="col_three_fifth nobottommargin">
                <div class="heading-block">
                    <h2>Why Accountswale.in</h2>
                </div>
<h3>Choosing us to make a difference</h3>
                <p>Income Tax Return ( ITR ) filing checklist for Salary Income /Individual in India.</p>			

                
            </div>

            <div class="col_two_fifth nobottommargin col_last">

                
                <a href="https://vimeo.com/101373765" data-lightbox="iframe">
                    <img src="{{URL::asset('UI/images/videos/1.jpg')}}" alt="Image">
                    <span class="i-overlay"><img src="{{URL::asset('UI/images/icons/play.png')}}" alt="Play"></span>
                </a>

            </div>

            <div class="clear"></div>

        </div>
        </section>


        <div class="container clearfix team">

            <div class="heading-block center">
                    <h2>Meet Our Mentors</h2>
                    <div class="bar"></div>
                    <span>How we serve you the best</span>
                </div>
            <!-- ======= Team Section ======= -->


<div class="row">

  <div class="col-lg-6">
    <div class="member d-flex align-items-start">
        <div class="timedate d-flex align-items-start">
            <div>27 July 2020</div>
            <div>Monday</div>
            <i>8:30PM</i>
        </div>
      <div class="pic"><img src="{{URL::asset('UI/images/trainers/trainer-1.jpg')}}" class="img-fluid" alt=""></div>
      <div class="member-info">
        <h4>Suresh Vankar</h4>
        <span>Accountant</span>
        <p>This webinar will help you to create a Powerful Perspective</p>
          <h5>Strategic Cost Management</h5>
        <div class="social">
          <a href=""><i class="icon-twitter"></i></a>
          <a href=""><i class="icon-facebook"></i></a>
          <a href=""><i class="icon-instagram"></i></a>
          <a href=""><i class="icon-linkedin-in"></i> </a>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6 mt-4 mt-lg-0">
    <div class="member d-flex align-items-start">
        <div class="timedate d-flex align-items-start">
            <div>28 July 2020</div>
            <div>Tuesday</div>
            <i>8:30PM</i>
        </div>
      <div class="pic"><img src="{{URL::asset('UI/images/trainers/trainer-2.jpg')}}" class="img-fluid" alt=""></div>
      <div class="member-info">
        <h4>Rajesh Sinha</h4>
        <span>GSTR Filing</span>
        <p>This webinar will help you to create a Powerful Perspective</p>
          <h5>Circumvent unwarranted expenses</h5>
        <div class="social">
          <a href=""><i class="icon-twitter"></i></a>
          <a href=""><i class="icon-facebook"></i></a>
          <a href=""><i class="icon-instagram"></i></a>
          <a href=""><i class="icon-linkedin-in"></i> </a>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6 mt-4">
    <div class="member d-flex align-items-start">
        <div class="timedate d-flex align-items-start">
            <div>29 July 2020</div>
            <div>Wednesday</div>
            <i>8:30PM</i>
        </div>
      <div class="pic"><img src="{{URL::asset('UI/images/trainers/trainer-3.jpg')}}" class="img-fluid" alt=""></div>
      <div class="member-info">
        <h4>Sarju Kaira</h4>
        <span>Yoga Courses</span>
        <p>This webinar will help you to create a Powerful Perspective</p>
          <h5>Lower financial expenditures</h5>
        <div class="social">
          <a href=""><i class="icon-twitter"></i></a>
          <a href=""><i class="icon-facebook"></i></a>
          <a href=""><i class="icon-instagram"></i></a>
          <a href=""><i class="icon-linkedin-in"></i> </a>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6 mt-4">
    <div class="member d-flex align-items-start">
        <div class="timedate d-flex align-items-start">
            <div>30 July 2020</div>
            <div>Thursday</div>
            <i>8:30PM</i>
        </div>
      <div class="pic"><img src="{{URL::asset('UI/images/trainers/trainer-4.jpg')}}" class="img-fluid" alt=""></div>
      <div class="member-info">
        <h4>Nishanth</h4>
        <span>Business Courses</span>
        <p>This webinar will help you to create a Powerful Perspective</p>
          <h5>Space Management</h5>
        <div class="social">
          <a href=""><i class="icon-twitter"></i></a>
          <a href=""><i class="icon-facebook"></i></a>
          <a href=""><i class="icon-instagram"></i></a>
          <a href=""><i class="icon-linkedin-in"></i> </a>
        </div>
      </div>
    </div>
  </div>

</div>


<!-- End Team Section -->

        </div>

        <div class="section topmargin-lg">
            <div class="container clearfix">
                <div class="heading-block center">
                    <h2>Knowledge Portal</h2>
                    <div class="bar"></div>
                    <span>Learn from our Videos and Blogs</span>
                </div>
                <div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">

                <div class="entry clearfix">
                    <div class="entry-image">
                        <a href="{{URL::asset('UI/images/blog/1.jpg')}}" data-lightbox="image"><img class="image_fade" src="{{URL::asset('UI/images/blog/1.jpg')}}" alt="Standard Post with Image"></a>
                    </div>
                    <ul class="entry-meta clearfix">
                        <li><a href="#"><i class="icon-male1"></i> Admin</a></li>
                        <li><i class="icon-calendar3"></i> 10th Feb 2014</li>								
                    </ul>							
                    
                    <div class="entry-content">	
                    <div class="entry-title">
                        <h2><a href="#">Cost Saving Techniques for SME’s: (Part -1)</a></h2>
                    </div>							
                        <a href="#" class="more-link">Read More</a>
                    </div>
                </div>
                    <div class="entry clearfix">
                    <div class="entry-image">
                        <a href="{{URL::asset('UI/images/blog/2.jpg')}}" data-lightbox="image"><img class="image_fade" src="{{URL::asset('UI/images/blog/2.jpg')}}" alt="Standard Post with Image"></a>
                    </div>
                    
                    <ul class="entry-meta clearfix">
                        <li><a href="#"><i class="icon-male1"></i> Admin</a></li>
                        <li><i class="icon-calendar3"></i> 10th Feb 2014</li>								
                    </ul>
                    <div class="entry-content">	
                        <div class="entry-title">
                        <h2><a href="#">Career Planning for the Finance Professional</a></h2>
                    </div>	
                        <a href="#" class="more-link">Read More</a>
                    </div>
                </div>
                    <div class="entry clearfix">
                    <div class="entry-image">
                        <a href="{{URL::asset('UI/images/blog/3.jpg')}}" data-lightbox="image"><img class="image_fade" src="{{URL::asset('UI/images/blog/3.jpg')}}" alt="Standard Post with Image"></a>
                    </div>
                    
                    <ul class="entry-meta clearfix">
                        <li><a href="#"><i class="icon-male1"></i> Admin</a></li>
                        <li><i class="icon-calendar3"></i> 10th Feb 2014</li>								
                    </ul>
                    <div class="entry-content">	
                    <div class="entry-title">
                        <h2><a href="#">Five Basic accounting tips for small business owners.</a></h2>
                    </div>							
                        <a href="#" class="more-link">Read More</a>
                    </div>
                </div>
            </div>
            </div>
        </div>

    </div>

</section>
@endsection