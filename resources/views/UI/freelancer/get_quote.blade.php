@extends('UI.base')
@section('Content')
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix pt-0">

            <div class="col_one_third nobottommargin">

                
    <div class="login_left">
        <div class="login_left_img"><img src="{{URL::asset('UI/images/update-bg.png')}}" alt="login background" style="height: 400px;"></div>
    </div>


            </div>

            <div class="col_two_third col_last nobottommargin">

<div class="heading-block left">
                    <h4>Get a Quote</h4>
                    <div class="bar" style="margin: 5px 0;"></div>
                    
                </div>
                

<!--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde, vel odio non dicta provident sint ex autem mollitia dolorem illum repellat ipsum aliquid illo similique sapiente fugiat minus ratione.</p>-->
                @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form id="register-form" name="register-form" class="nobottommargin" action="/freelancer/store_get_quote" method="post">
                    @csrf    
                    <div class="col_half">
                        <label for="register-form-fname">Name:</label>
                        <input type="hidden" id="register-form-name" name="user_id" value="{{Request::segment(3)}}" class="form-control" />
                        <input type="text" id="register-form-name" name="name" value="" class="form-control" />
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-email">Email Address:</label>
                        <input type="text" id="register-form-email" name="email" value="" class="form-control"/>
                    </div>

                    <div class="clear"></div>

                    <div class="col_half">
                        <label for="register-form-number">Contact Number:</label>
                        <input type="text" id="register-form-number" name="contact" value="" class="form-control"/>
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-city">City:</label>
                        <input type="text" id="register-form-city" name="city" value="" class="form-control" />	
                    </div>

                    <div class="clear"></div>

                    <div class="col_half">
                        <label for="register-form-country">Country:</label>
                        <input type="text" id="register-form-country" name="country" value="" class="form-control" />
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-state">State:</label>
                        <input type="text" id="register-form-state" name="state" value="" class="form-control" />
                    </div>

                    <div class="clear"></div>
                    <h4>Specific details of requirements</h4>
                        <div class="col_half">
                            <label for="register-form-services">When do you need this service ?:</label>
                            <select  id="register-form-services" name="service" value="" class="form-control" />
                                <option>Immediate </option>
                                <option>Within 1 Week</option>
                                <option>Within few weeks</option>
                                <option>within next 1-2 months</option>
                            </select>
                        </div>
                    <div class="col_half col_last">
                        <label for="register-form-client">Is this for your own company or for your client ?:</label>
                        <select  id="register-form-client" name="your_client" value="" class="form-control" />
                            <option>Own company</option>
                            <option>For client</option>
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div class="col_half">
                            <label for="register-form-services">Are you the decision maker for this requirement ?:</label>
                            <select  id="register-form-services" name="requirement" value="" class="form-control" />
                                <option>Yes</option>
                                <option>No</option>
                            </select>
                    </div>
                    <div class="col_half col_last">
                        <label for="register-form-budget">What is your Budget ?:</label>
<input type="text" id="register-form-budget" name="budget" value="" class="form-control" />
                    </div>

                    <div class="clear"></div>
                    <div class="col_half">
                            <label for="register-form-services">What is the best way to connect with you if we have any questions for your requirement ?:</label>
                            <select  id="register-form-services" name="questions_requirement" value="" class="form-control" />
                                <option>By Email</option>
                                <option>By direct phone call</option><option>By Whatsapp Message</option><option>By Whatsapp call</option>
                            </select>
                    </div>
                    <div class="clear"></div>
                    <div class="col_full nobottommargin">
                        <button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register" type="submit">Get a Quote</button>
                    </div>
                    </form>

                <br>
                <br>
                <br>
            </div>

        </div>

    </div>

</section>
@endsection