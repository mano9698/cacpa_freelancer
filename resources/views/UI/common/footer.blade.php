<footer id="footer" class="dark">

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            <div class="row align-items-center justify-content-between">
                <div class="col-md-5">
                    &copy; Copyrights 2020&nbsp;<a href="/">Cacpaworld.com</a>&nbsp;All rights reserved.</a> 
                    
                </div>

                <div class="col-md-7 tright">
                    <div>
                

                        <div class="clear"></div>
        
                        <i class="icon-envelope2"></i> info@cacpaworld.com <span class="middot">·</span> <i class="icon-headphones"></i> +91-99166 69702
                    </div>
                </div>
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer>