<header id="header" class="dark" data-sticky-class="not-dark">

    <div id="header-wrap">

        <div class="container clearfix">

            <!--<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>-->

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="#" class="standard-logo" data-dark-logo="{{URL::asset('UI/course/images/logo.png')}}"><img src="{{URL::asset('UI/course/images/logo.png')}}" alt="Canvas Logo"></a>
                <a href="#" class="retina-logo" data-dark-logo="{{URL::asset('UI/course/images/logo.png')}}"><img src="{{URL::asset('UI/course/images/logo.png')}}" alt="Canvas Logo"></a>
            </div><!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <!--<nav id="primary-menu">

                <ul>
                    <li class="current"><a href="#"><div>Home</div></a></li>
                    <li><a href="#"><div>About</div></a></li>						
                    <li><a href="#"><div>Courses</div></a></li>
                    <li><a href="#"><div>Blog</div></a>	</li>
                    
                </ul>


            </nav>--><!-- #primary-menu end -->

        </div>

    </div>

</header>