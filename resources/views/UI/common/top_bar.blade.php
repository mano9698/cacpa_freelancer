<div id="top-bar" class="bgcolor dark" style="z-index: 1;">

    <div class="container clearfix">

        <div class="row justify-content-between">
            <div class="col-md-2">
                

            </div>

            <div class="col-md-10 d-flex justify-content-center justify-content-md-end">

                <!-- Top Links
                ============================================= -->
<div class="top-links">
                @if(Auth::guard('user')->check())
                    <ul class="not-dark align-self-end">
                        <li>Go to profile</li>
                        <li><a href="/users/register"><i class="icon-registered1"></i> {{Session::get('Username')}}</a></li>
                    </ul>
                @else
                <ul class="sf-js-enabled clearfix" style="touch-action: pan-y;">
                    <!--<li><a href="register.php" data-lightbox="inline" class="color"><div><i class="icon-line2-login"></i>Login</div></a></li>-->
                    <li>For a freelancer  </li>
                    <li><a href="http://freelancer.taprecruiter.com/cacpa/register">Register</a></li>
                    <li class="d-none d-sm-inline-block"><a href="http://freelancer.taprecruiter.com/users/login"><i class="icon-line2-login"></i> Login</a></li>
                </ul>
                
                @endif
                    
                </div>

                <div id="top-social">
                    {{-- <ul>
                        <li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                        <!--<li><a href="#" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>-->
                        <li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                        <li><a href="tel:+919916669702" class="si-twitter"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+91-9916669702</span></a></li>
                        <li><a href="mailto:info@techitalents.com" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">info@techitalents.com</span></a></li>
                    </ul> --}}
                </div><!-- #top-social end -->

            </div>
        </div>

    </div>

</div>