@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Change User Password</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Change Your Password</strong></div>
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" action="/admin/update_password" method="post">
                @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Old Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" name="oldpassword">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">New Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" name="newpassword">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Confirm Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" name="password_confirmation">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <div class="col-sm-9 ml-auto">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Change Password</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection