<!DOCTYPE html>
<html>
  <head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>::Welcome to Accountswale:: {{$title}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{URL::asset('Admin/vendor/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{URL::asset('Admin/vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Custom Font Icons CSS-->
    <link rel="stylesheet" href="{{URL::asset('Admin/css/font.css')}}">
    <!-- Google fonts - Muli-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{URL::asset('Admin/css/style.default.css')}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{URL::asset('Admin/css/custom.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">

    <link rel="stylesheet" href="{{URL::asset('Admin/css/bootstrap-toggle.min.css')}}">

    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    @include('Admin.common.header')
    <div class="d-flex align-items-stretch">
      <!-- Sidebar Navigation-->
      @include('Admin.common.sidebar')
      <!-- Sidebar Navigation end-->
      @yield('Content')
    </div>
    <!-- JavaScript files-->
    <script src="{{URL::asset('Admin/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('Admin/vendor/popper.js/umd/popper.min.js')}}"> </script>
    <script src="{{URL::asset('Admin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('Admin/vendor/jquery.cookie/jquery.cookie.js')}}"> </script>
    <script src="{{URL::asset('Admin/vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{URL::asset('Admin/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{URL::asset('Admin/js/charts-home.js')}}"></script>
    <script src="{{URL::asset('Admin/js/front.js')}}"></script>

    <script src="{{URL::asset('Admin/js/custom/freelancer.js')}}"></script>

    <script src="{{URL::asset('Admin/js/bootstrap-toggle.min.js')}}"></script>

    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
  </body>
</html>