<?php

namespace App\Http\Controllers\Admin\Freelancer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\RequestCallBack;
use App\Models\UI\GetQuote;
use App\Models\UI\Users;
use Session;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class FreelancerController extends Controller
{
    public function request_call_back_lists(){
        $title ="Request Call Back";
        $UserId = Session::get('UserId');
        $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.freelancer.request_call_back_lists', compact('title', 'RequestCallBack'));
    }

    public function get_quote_lists(){
        $title ="Get Quote";
        $UserId = Session::get('UserId');
        $GetQuote = GetQuote::where('user_id', $UserId)->get();
        return view('Admin.freelancer.get_quote_lists', compact('title', 'GetQuote'));
    }


    public function edit_profile(){
        $title ="Edit Profile";
        $UserId = Session::get('UserId');
        $Users = Users::where('id', $UserId)->first();
        // echo json_encode($Users);
        // exit;
        return view('Admin.freelancer.edit_profile', compact('title', 'Users'));
    }
    
    public function update_profile(Request $request){

        $UserId = Session::get('UserId');

        $Users = Users::where('id', $UserId)->first();

        // echo $request->contact_no;
        // exit;

        // $Users->first_name = $request->first_name;
        // $Users->last_name = $request->last_name;
        // $Users->email = $request->email;
        // $Users->password = Hash::make($request->password);
        $Users->contact_no = $request->contact_no;
        $Users->gender = $request->gender;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;
        $Users->offers = $request->offers;
        $Users->area = $request->area;
        $Users->qualification = $request->qualification;
        $Users->experience_years = $request->experience_years;
        $Users->experience_months = $request->experience_months;
        $Users->skills = $request->skills;
        // $Users->skills = explode(",", $request->skills);

        if($request->hasfile('profile_pic')){
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $dir = 'Admin/freelancer/profile_pic';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('profile_pic')->move($dir, $filename);
    
            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Updated Successfully');

    }

    public function change_password(){
        $title ="Change Password";
        $UserId = Session::get('UserId');
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.freelancer.change_password', compact('title'));
    }


    public function update_password(Request $request)
    {
 
        //  $this->validate($request, [
 
        // 'oldpassword' => 'required',
        // 'newpassword' => 'required',
        // ]);
 
        $UserId = Session::get('UserId');
 
       $hashedPassword = Auth::guard('user')->user()->password;
 
       if (\Hash::check($request->oldpassword , $hashedPassword )) {
 
         if (!\Hash::check($request->newpassword , $hashedPassword)) {
 
              $users =Users::find($UserId);
              $users->password = bcrypt($request->newpassword);
              Users::where( 'id' , $UserId)->update( array( 'password' =>  $users->password));
 
              session()->flash('message','password updated successfully');
              return redirect()->back();
            }
 
            else{
                  session()->flash('message','new password can not be the old password!');
                  return redirect()->back();
                }
 
           }
 
          else{
               session()->flash('message','old password doesnt matched ');
               return redirect()->back();
             }
 
       }


       public function user_logout()
    {
        Auth::guard('user')->logout();
        return redirect('/');
    }
}
