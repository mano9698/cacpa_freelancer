<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use HasFactory;

    
    protected $table = 'users';

    use \Conner\Tagging\Taggable;

    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'contact_no', 'gender', 'country', 'state', 'city', 'area', 'pincode', 'offers', 'email_verified_at', 'qualification', 'experience_years', 'experience_months', 'skills', 'profile_pic', 'status', 'user_type'];
}
